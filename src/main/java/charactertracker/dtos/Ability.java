package charactertracker.dtos;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
class Ability {
    public String name;
    public String description;
    public String castingTime;
    public String range;
    public String duration;
}
