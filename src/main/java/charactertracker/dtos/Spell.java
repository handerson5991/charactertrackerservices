package charactertracker.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.simple.JSONObject;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "Spells")
public class Spell {
    public String name;
    public int level;
    public String school;
    public String type;
    public List<String> classes;
    public List<String> tags;
    public HashMap<String, String> components;
    public String description;
    public String casting_time;
    public String range;
    public String duration;
    public Boolean ritual;

//    public Spell(JSONObject obj) {
//        this.spellName = obj.get("Name").toString();
//        this.level = Integer.parseInt(obj.get("Level").toString());
//        this.school = obj.get("School").toString();
//        this.description = obj.get("data-description").toString();
//        this.castingTime = obj.get("Casting Time").toString();
//        this.range = obj.get("Range").toString();
//        this.duration = obj.get("Duration").toString();
//        this.damage = obj.get("Damage").toString();
//        this.damageType = obj.get("Damage Type").toString();
//        this.abilityModified = obj.get("abilityModified").toString();
//        this.abilityDice = obj.get("abilityDice").toString();
//    }
}
