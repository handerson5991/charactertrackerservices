package charactertracker.dtos;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "Notes")
public class Note {
    @Id
    @ApiModelProperty(hidden = true)
    public String id;
    public String characterName;
    public HashMap<String, String> notes;
}
