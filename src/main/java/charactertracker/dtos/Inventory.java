package charactertracker.dtos;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "Inventory")
public class Inventory {
    @Id
    @ApiModelProperty(hidden = true)
    public String id;
    public String characterName;
    public String userid;
    public int platinum;
    public int gold;
    public int silver;
    public int copper;
    public List<Item> items;
}
