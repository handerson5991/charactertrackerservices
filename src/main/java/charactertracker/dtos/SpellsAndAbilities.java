package charactertracker.dtos;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "Spells and Abilities")
public class SpellsAndAbilities {
    @Id
    @ApiModelProperty(hidden = true)
    public String id;
    public String characterName;
    public String userid;
    public String spellCastingAbility;
    public String spellSave;
    public String spellAttackBonus;
    public List<SpellSlot> spellSlots;
    public List<Spell> spells;
    public List<Ability> abilities;
}
