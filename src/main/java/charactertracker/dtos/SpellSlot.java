package charactertracker.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SpellSlot {
    public int level;
    public HashMap<String, Integer> slotTotals;
}
