package charactertracker.dtos;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "Items")
public class Item {
    public String name;
    public String description;
    public String type;
    public String damage;
    public String damageType;
    public String rarity;
    public String range;
    public List<String> properties;
    public int armorClass;
    public int quantity;
    public double weight;
}
