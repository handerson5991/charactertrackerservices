package charactertracker.dtos;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "Characters")
public class Character {
    @Id
    @ApiModelProperty(hidden = true)
    public String id;
    public String characterName;
    public String userid;
    public String backStory;
    public String race;
    public String background;
    public String alignment;
    public String personalityTraits;
    public String ideals;
    public String bonds;
    public String flaws;
    public String hitDice;
    public int inspiration;
    public int proficiency;
    public int armorClass;
    public int speed;
    public int hp;
    public int currentHp;
    public List<String> languages;
    public List<String> proficiencies;
    public HashMap<String, Integer> classes;
    public HashMap<String, Integer> stats;
    public HashMap<String, Integer> savingThrows;
}
