package charactertracker.controllers;

import charactertracker.dtos.SpellsAndAbilities;
import charactertracker.services.SpellsAndAbilitiesService;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SpellsAndAbilitiesController {
    private final SpellsAndAbilitiesService spellsAndAbilitiesService;

    public SpellsAndAbilitiesController(SpellsAndAbilitiesService spellsAndAbilitiesService) {
        this.spellsAndAbilitiesService = spellsAndAbilitiesService;
    }

    @GetMapping(value = "api/spellsAndAbilities/get")
    public ResponseEntity<?> getSpellsAndAbilities(@RequestParam String characterName, @RequestParam String userid) {
        return ResponseEntity.ok(spellsAndAbilitiesService.getSpellsAndAbilities(characterName, userid));
    }

    @PostMapping(value = "api/spellsAndAbilities/add")
    public ResponseEntity<?> addSpellsAndAbilities(@RequestBody SpellsAndAbilities spellsAndAbilities) {
        return ResponseEntity.ok(spellsAndAbilitiesService.addSpellsAndAbilities(spellsAndAbilities));
    }

    @PutMapping(value = "api/spellsAndAbilities/update")
    public ResponseEntity<?> updateSpellsAndAbilities(@RequestBody SpellsAndAbilities spellsAndAbilities) {
        return ResponseEntity.ok(spellsAndAbilitiesService.addSpellsAndAbilities(spellsAndAbilities));
    }

    @DeleteMapping(value = "api/spellsAndAbilities/delete")
    public ResponseEntity<?> deleteSpellsAndAbilities(@RequestParam String characterName, @RequestParam String userid) {
        spellsAndAbilitiesService.deleteSpellsAndAbilities(characterName, userid);
        return ResponseEntity.ok().build();
    }
}
