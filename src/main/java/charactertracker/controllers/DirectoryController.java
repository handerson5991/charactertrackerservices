package charactertracker.controllers;

import charactertracker.services.DirectoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DirectoryController {
    private final DirectoryService directoryService;

    public DirectoryController(DirectoryService directoryService) {
        this.directoryService = directoryService;
    }

    @GetMapping(value = "api/directory/getItem/{item}")
    public ResponseEntity<?> getItem(@PathVariable String item) {
        return ResponseEntity.ok(directoryService.getItem(item));
    }

    @PostMapping(value = "api/directory/getItems")
    public ResponseEntity<?> getItems(@RequestBody List<String> items) {
        return ResponseEntity.ok(directoryService.getItems(items));
    }

    @PostMapping(value = "api/directory/getSpells/{_class}")
    public ResponseEntity<?> getSpellsByClassAndLevel(@PathVariable String _class, @RequestBody List<Integer> levels) {
        return ResponseEntity.ok(directoryService.getSpellsByClassAndLevel(_class.toLowerCase(), levels));
    }
}
