package charactertracker.controllers;

import charactertracker.dtos.Inventory;
import charactertracker.services.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class InventoryController {

    private final InventoryService inventoryService;

    public InventoryController(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

    @GetMapping(value = "api/inventory/get")
    public ResponseEntity<?> getInventory(@RequestParam String characterName, @RequestParam String userid) {
        return ResponseEntity.ok(inventoryService.getInventory(characterName, userid));
    }

    @PostMapping(value = "api/inventory/add")
    public ResponseEntity<?> addInventory(@RequestBody Inventory inventory) {
        return ResponseEntity.ok(inventoryService.addInventory(inventory));
    }

    @PutMapping(value = "api/inventory/update")
    public ResponseEntity<?> updateInventory(@RequestBody Inventory inventory) {
        return ResponseEntity.ok(inventoryService.addInventory(inventory));
    }

    @DeleteMapping(value = "api/inventory/delete")
    public ResponseEntity<?> deleteInventory(@RequestParam String characterName, @RequestParam String userid) {
        inventoryService.deleteInventory(characterName, userid);
        return ResponseEntity.ok().build();
    }
}
