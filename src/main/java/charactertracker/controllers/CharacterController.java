package charactertracker.controllers;

import charactertracker.dtos.Character;
import charactertracker.services.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CharacterController {
    private final CharacterService characterService;

    public CharacterController(CharacterService characterService) {
        this.characterService = characterService;
    }

    @GetMapping(value = "api/character/get")
    public ResponseEntity<?> getCharacter(@RequestParam String characterName, @RequestParam String userid) {
        return ResponseEntity.ok(characterService.getCharacter(characterName, userid));
    }

    @PostMapping(value = "api/character/add")
    public ResponseEntity<?> addCharacter(@RequestBody Character character) {
        return ResponseEntity.ok(characterService.addCharacter(character));
    }

    @PutMapping(value = "api/character/update")
    public ResponseEntity<?> updateCharacter(@RequestBody Character character) {
        return ResponseEntity.ok(characterService.addCharacter(character));
    }

    @DeleteMapping(value = "api/character/delete")
    public ResponseEntity<?> deleteCharacter(@RequestParam String characterName, @RequestParam String userid) {
        characterService.deleteCharacter(characterName, userid);
        return ResponseEntity.ok().build();
    }
}
