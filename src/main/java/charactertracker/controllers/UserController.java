package charactertracker.controllers;

import charactertracker.dtos.User;
import charactertracker.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "api/user/get")
    public ResponseEntity<?> getUser(@RequestParam String userid, @RequestParam String password) {
        return ResponseEntity.ok(userService.getUser(userid, password));
    }

    @PostMapping(value = "api/user/add")
    public ResponseEntity<?> addUser(@RequestBody User user) {
        return ResponseEntity.ok(userService.addUser(user));
    }

    @PutMapping(value = "api/user/update/{userid}")
    public ResponseEntity<?> updateUser(@PathVariable String userid, @RequestBody List<String> characters) {
        return ResponseEntity.ok(userService.updateUserCharacters(userid, characters));
    }

    @DeleteMapping(value = "api/user/delete")
    public ResponseEntity<?> deleteUser(@RequestParam String userid) {
        userService.deleteUser(userid);
        return ResponseEntity.ok().build();
    }
}
