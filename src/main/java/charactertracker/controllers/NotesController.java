package charactertracker.controllers;

import charactertracker.dtos.Note;
import charactertracker.services.NotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NotesController {
    private final NotesService notesService;

    public NotesController(NotesService notesService) {
        this.notesService = notesService;
    }

    @GetMapping(value = "api/note/get")
    public ResponseEntity<?> getNotes(@RequestParam String characterName) {
        return ResponseEntity.ok(notesService.getNotes(characterName));
    }

    @PostMapping(value = "api/note/add")
    public ResponseEntity<?> addNote(@RequestBody Note note) {
        return ResponseEntity.ok(notesService.addNote(note));
    }

    @PutMapping(value = "api/note/update")
    public ResponseEntity<?> updateNote(@RequestBody Note note) {
        return ResponseEntity.ok(notesService.addNote(note));
    }

    @DeleteMapping(value = "api/note/delete")
    public ResponseEntity<?> deleteNotes(@RequestParam String characterName) {
        notesService.deleteNotes(characterName);
        return ResponseEntity.ok().build();
    }
}
