package charactertracker.config;

import charactertracker.services.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class AppConfig {
    @Value("${roll20Url}")
    private String roll20Url;

    @Bean
    public CharacterService characterService() {
        return new CharacterService();
    }

    @Bean
    public InventoryService inventoryService() {
        return new InventoryService();
    }

    @Bean
    public SpellsAndAbilitiesService spellsAndAbilitiesService() {
        return new SpellsAndAbilitiesService();
    }

    @Bean
    public NotesService notesService() {
        return new NotesService();
    }

    @Bean
    public UserService userService() {
        return new UserService();
    }

    @Bean
    public DirectoryService directoryService() {
        return new DirectoryService();
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("http://localhost:4200",
                                "http://charactertrackerweb.s3-website-us-west-1.amazonaws.com",
                                "http://charactertrackerweb-dev.s3-website-us-west-1.amazonaws.com")
                        .allowedMethods("PUT", "POST", "GET", "DELETE");
            }
        };
    }
}
