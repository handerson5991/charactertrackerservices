package charactertracker.daos;

import charactertracker.dtos.SpellsAndAbilities;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SpellsAndAbilitiesRepo extends MongoRepository<SpellsAndAbilities, String> {
    SpellsAndAbilities findOneByCharacterNameAndUserid(String name, String userid);

    SpellsAndAbilities save(SpellsAndAbilities spellsAndAbilities);

    void deleteOneByCharacterNameAndUserid(String name, String userid);
}
