package charactertracker.daos;

import charactertracker.dtos.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepo extends MongoRepository<User, String> {
    User findOneByUseridAndPassword(String userid, String password);

    User findOneByUserid(String userid);

    User save(User user);

    void deleteOneByUserid(String userid);
}
