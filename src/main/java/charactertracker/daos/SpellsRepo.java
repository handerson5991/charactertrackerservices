package charactertracker.daos;

import charactertracker.dtos.Spell;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface SpellsRepo extends MongoRepository<Spell, String> {
    Spell findByName(String name);

    List<Spell> findAllByClassesInAndLevelIn(String _class, List<Integer> levels);
}
