package charactertracker.daos;

import charactertracker.dtos.Inventory;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface InventoryRepo extends MongoRepository<Inventory, String> {
    Inventory findOneByCharacterNameAndUserid(String name, String userid);

    Inventory save(Inventory inventory);

    void deleteOneByCharacterNameAndUserid(String name, String userid);
}
