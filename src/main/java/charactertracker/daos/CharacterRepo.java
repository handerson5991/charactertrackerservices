package charactertracker.daos;

import charactertracker.dtos.Character;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CharacterRepo extends MongoRepository<Character, String> {
    Character findOneByCharacterNameAndUserid(String name, String userid);

    Character save(Character character);

    void deleteOneByCharacterNameAndUserid(String name, String userid);
}
