package charactertracker.daos;

import charactertracker.dtos.Note;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NotesRepo extends MongoRepository<Note, String> {
    Note findOneByCharacterName(String name);

    Note save(Note note);

    void deleteOneByCharacterName(String name);
}
