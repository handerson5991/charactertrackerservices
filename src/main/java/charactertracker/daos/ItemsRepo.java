package charactertracker.daos;

import charactertracker.dtos.Item;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ItemsRepo extends MongoRepository<Item, String> {
    Item findByName(String name);

    List<Item> findAllByNameIn(List<String> names);
}
