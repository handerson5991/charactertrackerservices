package charactertracker.services;

import charactertracker.daos.InventoryRepo;
import charactertracker.dtos.Inventory;
import org.springframework.beans.factory.annotation.Autowired;

public class InventoryService {

    @Autowired
    private InventoryRepo inventoryRepo;

    public Inventory getInventory(String characterName, String userid) {
        return inventoryRepo.findOneByCharacterNameAndUserid(characterName, userid);
    }

    public Inventory addInventory(Inventory inventory) {
        return inventoryRepo.save(inventory);
    }

    public void deleteInventory(String characterName, String userid) {
        inventoryRepo.deleteOneByCharacterNameAndUserid(characterName, userid);
    }
}
