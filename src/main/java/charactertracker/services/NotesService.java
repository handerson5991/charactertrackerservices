package charactertracker.services;

import charactertracker.daos.NotesRepo;
import charactertracker.dtos.Note;
import org.springframework.beans.factory.annotation.Autowired;

public class NotesService {

    @Autowired
    private NotesRepo notesRepo;

    public Note getNotes(String characterName) {
        return notesRepo.findOneByCharacterName(characterName);
    }

    public Note addNote(Note note) {
        return notesRepo.save(note);
    }

    public void deleteNotes(String characterName) {
        notesRepo.deleteOneByCharacterName(characterName);
    }
}
