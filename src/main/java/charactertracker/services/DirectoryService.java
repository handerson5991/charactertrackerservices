package charactertracker.services;

import charactertracker.daos.ItemsRepo;
import charactertracker.daos.SpellsRepo;
import charactertracker.dtos.Item;
import charactertracker.dtos.Spell;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class DirectoryService {

    @Autowired
    private SpellsRepo spellsRepo;

    @Autowired
    private ItemsRepo itemsRepo;

    public Item getItem(String name) {
        return itemsRepo.findByName(name);
    }

    public List<Item> getItems(List<String> names) {
        return itemsRepo.findAllByNameIn(names);
    }

    public List<Spell> getSpellsByClassAndLevel(String _class, List<Integer> levels) {
        return spellsRepo.findAllByClassesInAndLevelIn(_class, levels);
    }
}
