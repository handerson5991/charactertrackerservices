package charactertracker.services;

import charactertracker.daos.CharacterRepo;
import charactertracker.dtos.Character;
import org.springframework.beans.factory.annotation.Autowired;

public class CharacterService {

    @Autowired
    private CharacterRepo characterRepo;

    public Character getCharacter(String characterName, String userid) {
        return characterRepo.findOneByCharacterNameAndUserid(characterName, userid);
    }

    public Character addCharacter(Character character) {
        return characterRepo.save(character);
    }

    public void deleteCharacter(String characterName, String userid) {
        characterRepo.deleteOneByCharacterNameAndUserid(characterName, userid);
    }
}
