package charactertracker.services;

import charactertracker.daos.UserRepo;
import charactertracker.dtos.User;
import charactertracker.exceptions.DuplicateException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class UserService {

    @Autowired
    private UserRepo userRepo;

    public User getUser(String userid, String password) {
        return userRepo.findOneByUseridAndPassword(userid, password);
    }

    public User addUser(User user) {
        if (getUser(user.userid, user.password) != null)
            throw new DuplicateException("Userid already exists");
        return userRepo.save(user);
    }

    public User updateUserCharacters(String userid, List<String> characters) {
        User user = userRepo.findOneByUserid(userid);
        user.characters = characters;
        return userRepo.save(user);
    }

    public void deleteUser(String userid) {
        userRepo.deleteOneByUserid(userid);
    }
}
