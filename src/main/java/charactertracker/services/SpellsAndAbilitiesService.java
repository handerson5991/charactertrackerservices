package charactertracker.services;

import charactertracker.daos.SpellsAndAbilitiesRepo;
import charactertracker.dtos.SpellsAndAbilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

public class SpellsAndAbilitiesService {

    @Autowired
    private SpellsAndAbilitiesRepo spellsAndAbilitiesRepo;

    @Autowired
    RestTemplate restTemplate;

    public SpellsAndAbilitiesService() {
    }

    public SpellsAndAbilities getSpellsAndAbilities(String characterName, String userid) {
        return spellsAndAbilitiesRepo.findOneByCharacterNameAndUserid(characterName, userid);
    }

    public SpellsAndAbilities addSpellsAndAbilities(SpellsAndAbilities spellsAndAbilities) {
        spellsAndAbilitiesRepo.save(spellsAndAbilities);
        return getSpellsAndAbilities(spellsAndAbilities.characterName, spellsAndAbilities.userid);
    }

    public void deleteSpellsAndAbilities(String characterName, String userid) {
        spellsAndAbilitiesRepo.deleteOneByCharacterNameAndUserid(characterName, userid);
    }
}
